require 'json'

#
# @summary Gets informations about Nextcloud apps
#
Facter.add('nextcloud_apps') do
  confine kernel: 'Linux'
  setcode do
    nextcloud_dir = '/var/nextcloud-app/nextcloud'
    occ = "#{nextcloud_dir}/occ"
    if File.exist?(nextcloud_dir) && File.exist?(occ) && Facter::Util::Resolution.which('php')
      Dir.chdir(nextcloud_dir) do
        uid = File.stat(occ).uid
        JSON.parse(Puppet::Util::Execution.execute('php occ app:list --ansi --output=json', uid: uid))
      end
    else
      nil
    end
  end
end
