Puppet::Type.newtype(:nextcloud_application) do
  @doc = <<-MANIFEST
    @summary This type provides Puppet with the capabilities to manage Nextcloud apps
    @example Basic usage
      nextcloud_application { 'app_example':
      }
    @api private
  MANIFEST

  ensurable

  newparam(:name, namevar: true) do
    desc 'Name of app'
  end
end
