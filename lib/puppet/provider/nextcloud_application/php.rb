require File.expand_path(File.join(File.dirname(__FILE__), '..', 'nextcloud.rb'))

Puppet::Type.type(:nextcloud_application).provide(:php, parent: Puppet::Provider::Nextcloud) do
  desc 'provider for nextcloud_application resource'

  confine kernel: 'Linux'
  defaultfor kernel: 'Linux'
  commands php: 'php'

  def self.instances
    apps = Facter.value(:nextcloud_apps)
    if apps
      apps['enabled'].map do |name, _version|
        # initializes @property_hash
        new(
          name: name,
          ensure: :present,
        )
      end
    else
      []
    end
  end

  def self.prefetch(resources)
    # discover all nextcloud_application resources on the system by invoking self.instances
    apps = instances
    # iterates through all nextcloud_application resources in the catalog
    resources.keys.each do |name|
      # if the managed app exists in the self.instances cache,
      # assigns its provider to set its property hash
      provider = apps.find { |app| app.name == name }
      resources[name].provider = provider if provider
    end
  end

  def exists?
    @property_hash[:ensure] == :present
  end

  def create
    execute_occ("app:install #{resource[:name]}")
  end

  def destroy
    execute_occ("app:remove #{resource[:name]}")
  end
end
