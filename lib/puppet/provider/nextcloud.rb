# Some factorized code for all providers
class Puppet::Provider::Nextcloud < Puppet::Provider
  def self.execute_occ(occ_params)
    # occ tool is provided by Nextcloud.
    # It is required to execute occ :
    # * with current working directory in directory where occ is present
    # * run as occ's owner
    nextcloud_dir = '/var/nextcloud-app/nextcloud'
    occ = "#{nextcloud_dir}/occ"

    raise Puppet::Error, 'Could not find Nextcloud directory or Nextcloud CLI' unless File.exist?(nextcloud_dir) && File.exist?(occ)

    Dir.chdir(nextcloud_dir) do
      uid = File.stat(occ).uid
      begin
        Puppet::Util::Execution.execute("php occ #{occ_params}", uid: uid)
      rescue Puppet::ExecutionFailure => e
        raise Puppet::Error, "Could not execute 'php occ #{occ_params}' : #{e.inspect}"
      end
    end
  end

  def execute_occ(*args)
    self.class.execute_occ(*args)
  end
end
