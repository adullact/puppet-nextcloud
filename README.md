# nextcloud

Install and configure Nextcloud instances.

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with nextcloud](#setup)
    * [What nextcloud affects](#what-nextcloud-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nextcloud](#beginning-with-nextcloud)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

The module installs and configures Nextcloud. It can also extend Nextcloud by installing applications from Nextcloud  store.

The 1.x versions of this module made it possible to install several instances of Nexcloud on a same node.
But, this feature is removed for version 2.x and upper. Now, it is only possible to install one instance on a node.

## Setup

### What nextcloud affects

This module downloads Nextcloud and applications listed in `additional_apps`. Nextcloud can be extended by awesome
 apps. Perhaps the following list can be interesting for you : polls, calendar, unsplash, deck, tasks, contacts, forms, groupfolders, terms_of_service, spreed.

This module can install php packages provided by OS and enable caching with redis.

Some settings can be defined like the `datadirectory`. This path stores all uploaded files by users. It will grow up.

### Setup Requirements

The cron service have to be enabled. With Puppet 6, you have to install one more dependency [puppetlabs-cron_core](https://forge.puppet.com/puppetlabs/cron_core).

Before using this module :
 * You have to know address of database server, database name with user account and password. The module will connect to the database service and populate tables to obtain a working nextcloud instance. You can use [puppetlabs-mysql](https://forge.puppet.com/puppetlabs/mysql) to build MySQL or MariaDB service.
 * You have to provide a web server. You can use [puppet-nginx](https://forge.puppet.com/puppet/nginx) or [puppetlabs-apache](https://forge.puppet.com/puppetlabs/apache). Use value `/var/nextcloud-app/nextcloud`, as `DocumentRoot` of Nextcloud's `VirtualHost` directive for Apache, or as `root` of Nextcloud's `server` block for Nginx.
 * The module installs php packages provided by OS. If you prefer using PHP with FMP, you have to set `manage_phplibs` to `false`. You can then have a look to [puppet-php](https://forge.puppet.com/puppet/php) module.

### Beginning with nextcloud

To install Nextcloud on the Puppet node, with database service provided on `127.0.0.1` :

```
  class { 'nextcloud' :
    database_name     => 'nextcloud_example',
    database_user     => 'user_example',
    database_password => 'secret',
    system_user       => 'www-data',
    config            => {
      trusted_domains => [
        'localhost',
        'cloud.example.com',
      ],
    }
  }
```

## Usage

During the install process one administrator account is created named `admin`, with password `changeme`.

The application is installed in `/var/nextcloud-app`.
The data uploaded by users is, by default, in `/var/nextcloud-data` and can be modified by parameter `$config::datadirectory`.
If you modify the `datadirectory` path on an already running Nextcloud instance, the configuration will be updated but the
data will not be moved and will stay in the previous path.

All config settings handled by parameter `$config` makes accordingly updates into file `config.php`.
But removing a setting from `$config` does not remove it from `config.php` (`ensure => absent` does not exists).

### The default values of `$config` settings are following :

```
    'datadirectory'        => '/var/nextcloud-data',
    'trusted_domains'      => ['localhost', $facts['networking']['fqdn']],
    'memcache.local'       => '\OC\Memcache\Redis',
    'memcache.distributed' => '\OC\Memcache\Redis',
    'memcache.locking'     => '\OC\Memcache\Redis',
    'filelocking.enabled'  => true,
    'redis'                => {
      'host'    => '127.0.0.1',
      'port'    => 6379,
      'timeout' => '1.5',
    },
```

### Configure an instance with mail with STARTTLS connexion on smtp server :

```
  class { 'nextcloud' :
    database_name     => 'nextcloud_example',
    database_user     => 'user_example',
    database_password => 'secret',
    system_user       => 'www-data',
    config            => {
      trusted_domains     => [
        'localhost',
        'cloud.example.com',
      ],
      'mail_smtpmode'     => 'smtp',
      'mail_smtphost'     => 'smtp.example.com',
      'mail_smtpport'     => 425,
      'mail_smtpsecure'   => 'tls',
      'mail_smtpauth'     => true,
      'mail_smtpauthtype' => 'LOGIN',
      'mail_smtpname'     => 'username',
      'mail_smtppassword' => 'password',
    }
  }
```

See more details in mail settings [Nextcloud Configuration > Email](https://docs.nextcloud.com/server/18/admin_manual/configuration_server/email_configuration.html)

You can also have a look to custom Puppet data type [Nextcloud::Config](https://gitlab.adullact.net/adullact/puppet-nextcloud/-/blob/master/REFERENCE.md#nextcloudconfig) in `REFERENCE.md`.

### Notes about upgrade from 1.x to 2.x

If you are using this module in version 1.x to configure several instances of Nextcloud on the same Puppet node, this is no longer possible with the version 2.x and higher.

To upgrade this Puppet module applied on a running node, you can :
 * disable temporary the Puppet agent with `puppet agent --disable`,
 * stop the web server,
 * rename the directory `/var/nextcloud-<fqdn>-<version>` into `/var/nextcloud-app`,
 * upgrade Puppet module from 1.x to 2.x (warning: default value of `version` has changed),
 * check that all is ready with `puppet agent --test --noop`,
 * re-enable the Puppet agent with `puppet agent --enable`.

## Reference

Details in [REFERENCE.md](https://gitlab.adullact.net/adullact/puppet-nextcloud/-/blob/master/REFERENCE.md).

The module provides custom facts :
 * `nextcloud`:  Gives status informations of Nextcloud instance.
 * `nextcloud_apps`: Gives informations of Nextcloud apps.

## Limitations

Compatible OSes, dependencies and Puppet versions supported are given in [metadata.json](https://gitlab.adullact.net/adullact/puppet-nextcloud/-/blob/master/metadata.json).

## Development

You can get some guidance in [CONTRIBUTING.md](https://gitlab.adullact.net/adullact/puppet-nextcloud/-/blob/master/CONTRIBUTING.md).

## Release Notes/Contributors/Etc.

All notable changes to this module are documented in [CHANGELOG.md](https://gitlab.adullact.net/adullact/puppet-nextcloud/-/blob/master/CHANGELOG.md).

This module puppet-nextcloud is maintained by [ADULLACT](https://adullact.org), it was written by Fabien 'Dan33l' Combernous.
