# @summary Configure Nextcloud instance
#
# Configure Nextcloud instance
#
# @param config : Hash corresponding to config.php settings
# @param appdirectory : Directory where Nextcloud is installed
#
# @api private
#
class nextcloud::config (
  Hash $config,
  String $appdirectory,
) {
  assert_private()

  $config.each | $_config_key, $_config_value | {
    case $_config_value {
      String, Boolean, Integer: {
        exec { "occ config:system:set ${_config_key}":
          path    => '/usr/sbin:/usr/bin:/sbin:/bin',
          command => "php occ config:system:set ${_config_key} --value='${_config_value}'",
          cwd     => "${appdirectory}/nextcloud",
          user    => $nextcloud::system_user,
          unless  => "php occ config:system:get ${_config_key} | grep -qF '${_config_value}'",
        }
      }
      Array: {
        $_config_value.each | $_index, $_config_array_value | {
          exec { "occ config:system:set ${_config_key} index ${_index}":
            path    => '/usr/sbin:/usr/bin:/sbin:/bin',
            command => "php occ config:system:set ${_config_key} ${_index} --value='${_config_array_value}'",
            cwd     => "${appdirectory}/nextcloud",
            user    => $nextcloud::system_user,
            unless  => "php occ config:system:get ${_config_key} | grep -qF '${_config_array_value}'",
          }
        }
      }
      Hash: {
        $_config_value.each | $_config_hash_key, $_config_hash_value | {
          exec { "occ config:system:set ${_config_key} key ${_config_hash_key}":
            path    => '/usr/sbin:/usr/bin:/sbin:/bin',
            command => "php occ config:system:set ${_config_key} '${_config_hash_key}' --value='${_config_hash_value}'",
            cwd     => "${appdirectory}/nextcloud",
            user    => $nextcloud::system_user,
            unless  => "php occ config:system:get ${_config_key} ${_config_hash_key} | grep -qF '${_config_hash_value}'",
          }
        }
      }
      default: {
        fail('unexpected data type in config')
      }
    }
  }
}
