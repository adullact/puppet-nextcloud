# @summary Handle nextcloud installation
#
# Handle nextcloud installation
#
# @param appdirectory Directory where Nextcloud is installed
# @param installcmd occ command used to install Nextcloud and populate database.
#
# @api private
#
class nextcloud::install (
  String $appdirectory,
  String $installcmd,
){
  assert_private()

  if !$facts['nextcloud'] or !$facts['nextcloud']['installed'] {
    exec { 'occ maintenance:install':
      path    => '/usr/sbin:/usr/bin:/sbin:/bin',
      command => $installcmd,
      cwd     => "${appdirectory}/nextcloud",
      user    => $nextcloud::system_user,
      require => Archive["nextcloud-${nextcloud::version}"],
    }

    -> exec { 'occ db:add-missing-indices':
      path        => '/usr/sbin:/usr/bin:/sbin:/bin',
      command     => 'php occ db:add-missing-indices --no-interaction',
      cwd         => "${appdirectory}/nextcloud",
      user        => $nextcloud::system_user,
      refreshonly => true,
    }

    -> exec { 'occ db:convert-filecache-bigint':
      path        => '/usr/sbin:/usr/bin:/sbin:/bin',
      command     => 'php occ db:convert-filecache-bigint --no-interaction',
      cwd         => "${appdirectory}/nextcloud",
      user        => $nextcloud::system_user,
      refreshonly => true,
    }
  }
}
