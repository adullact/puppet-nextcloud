# @summary Install and configure Nextcloud
#
# Install and configure Nextcloud
#
# @example
#  class { 'nextcloud':
#    version           => '18.0.2',
#    database_host     => '10.0.0.10',
#    database_name     => 'nextcloud_dnname',
#    database_user     => 'nextcloud_dbuser',
#    database_password => 'secret',
#    additional_apps   => ['unsplash'],
#  }
#
# @param archive_url URL from which the Nextcloud archive will be downloaded
# @param manage_phplibs
#    false: you have to manage somewhere else the required php libs.
#    true: the module installs php packages (which are listed in phplibs)
# @param phplibs If manage_phplibs is true, list of php packages to be installed.
# @param manage_redis
#    false: the module does not install redis on the node.
#    true: a redis service is installed on the node and bound to 127.0.0.1
# @param system_user System account existong on node that own Nextcloud files.
# @param version Version of installed Nexcloud.
# @param additional_apps Nextcloud can be extended by applications. You can give a list a additional applications here.
# @param database_driver The database engine used as backend.
# @param database_host Host address of the database used as backend.
# @param database_name Name of the database used as backend.
# @param database_user User accound used to connect the database.
# @param database_password Password used to connect the database
# @param config Content of config.php, details of handled settings described in custom data type Nextcloud::Config and Nextcloud::Config::Redis.
#
class nextcloud (
  String[1] $database_password,
  Stdlib::HTTPUrl $archive_url = 'https://download.nextcloud.com/server/releases/',
  Boolean $manage_phplibs = true,
  Boolean $manage_redis = true,
  Array $phplibs = ['php7.2','php7.2-zip','php7.2-xml','php7.2-mbstring','php7.2-curl','php7.2-gd','php7.2-json','php-imagick','php7.2-intl','php-redis'],
  Enum['mysql'] $database_driver = 'mysql',
  String $database_name = 'nextcloud',
  String $database_user = 'nextcloud',
  Stdlib::Host $database_host = '127.0.0.1',
  String $system_user = 'www-data',
  String $version = '18.0.3',
  Array[String] $additional_apps = [],
  Nextcloud::Config $config = {},
) {

  if $manage_phplibs {
    $_driver_package = $database_driver ? { 'mysql' => 'php7.2-mysql', 'pgsql' => 'php7.2-pgsql' }
    $_redis_package = 'php-redis'

    package { $_driver_package :
      ensure => present,
    }
    package { $phplibs :
      ensure => present,
    }

    ensure_packages( $_redis_package, { ensure => present } )
  }

  if $manage_redis {
    include redis
  }

  package { 'unzip':
    ensure => present,
  }

  $_nextcloud_appdirectory = '/var/nextcloud-app'

  $_default_config = {
    'dbhost'               => $database_host,
    'dbname'               => $database_name,
    'dbuser'               => $database_user,
    'dbpassword'           => $database_password,
    'datadirectory'        => '/var/nextcloud-data',
    'trusted_domains'      => ['localhost', $facts['networking']['fqdn']],
    'memcache.local'       => '\OC\Memcache\Redis',
    'memcache.distributed' => '\OC\Memcache\Redis',
    'memcache.locking'     => '\OC\Memcache\Redis',
    'filelocking.enabled'  => true,
    'redis'                => {
      'host'    => '127.0.0.1',
      'port'    => 6379,
      'timeout' => '1.5',
    },
  }

  $_real_config = $_default_config + $config

  $_installcmd = "php occ maintenance:install --database '${database_driver}' \
 --database-host ${database_host} \
 --database-name ${database_name} \
 --database-user ${database_user} \
 --database-pass '${database_password}' \
 --admin-user admin \
 --admin-pass changeme \
 --data-dir ${_real_config['datadirectory']}"

  if $manage_phplibs {
    Package[$phplibs] -> Class['nextcloud::install']
  }

  if $manage_redis and $manage_phplibs {
    Class['redis'] -> Package[$_redis_package] -> Class['nextcloud::install']
  } elsif $manage_redis {
    Class['redis'] -> Class['nextcloud::install']
  }

  file { [ $_nextcloud_appdirectory, $_real_config['datadirectory'] ]:
    ensure => directory,
    owner  => $system_user,
    group  => $system_user,
    mode   => '0700',
  }

  file { "${_real_config['datadirectory']}/.ocdata":
    ensure =>  file,
    owner  => $system_user,
    group  => $system_user,
    mode   => '0644',
  }

  archive { "nextcloud-${version}":
    path         => "/tmp/nextcloud-${version}.zip",
    source       => "${archive_url}/nextcloud-${version}.zip",
    extract      => true,
    extract_path => $_nextcloud_appdirectory,
    creates      => "${_nextcloud_appdirectory}/nextcloud/index.php",
    cleanup      => true,
    user         => $system_user,
    group        => $system_user,
    require      => [
      Package['unzip'],
      File[$_nextcloud_appdirectory],
      File[$_real_config['datadirectory']],
    ],
  }

  -> class{ 'nextcloud::install' :
    appdirectory => $_nextcloud_appdirectory,
    installcmd   => $_installcmd,
  }

  -> class { 'nextcloud::config':
    config       => $_real_config,
    appdirectory => $_nextcloud_appdirectory,
    require      => Class['nextcloud::install'],
  }

  $nextcloud::additional_apps.each | String $_app_name | {
    nextcloud_application{ $_app_name:
      ensure  => present,
      require => Class['nextcloud::config'],
    }
  }

  cron { 'background_jobs':
    command => "php -f ${_nextcloud_appdirectory}/nextcloud/cron.php",
    user    => $system_user,
    minute  => '*/5',
  }
}
