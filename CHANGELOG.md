# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and the project follows [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Release 2.1.2 — 2020-04-12

 * minor documentation fixes #44
 * fix provider , call raise in function execute_occ #43
 * mutualize some code for providers #42

## Release 2.1.1 — 2020-04-10

  * update README about webserver requirement #40
  * fix doc about database and example #39

## Release 2.1.0 — 2020-04-09

  * use custom fact to trigger install #37
  * add type/provider `nextcloud_application` #35
  * add custom fact to get informations about installed nextcloud apps #34

## Release 2.0.0 — 2020-03-31

### Beaking changes

  * remove version from path to nextcloud directory #29
  * remove feature permiting to install several instances on same node #26

### Closed issues

  * complete the upgrade note from 1.x to 2.x #32
  * update README.md to give information from 1.x to 2.x #30
  * move default version number to 18.0.3 #28
  * add custom fact to get occ status #7

## Release 1.0.4 — 2020-03-24

 * update README to announce futur breaking change #24
 * some sensitive data can be displayed during run #23 (security)

## Release 1.0.3 — 2020-03-19

 * DOC Have more precise description of parameters #16
 * Fix metadata about puppet5 compatibility, regenerate `REFERENCE.md` #20
 * Fix Unknown variable: `nextcloud::_redis_package` when `manage_phplibs` to false #21

## Release 1.0.2 — 2020-03-18

 * Add release process #15, #18

## Release 1.0.1 — 2020-03-18

 * migrate to adullact namespace #14, #13, #12
 * Fix typos #11

## Release 1.0.0 — 2020-03-16

Initial release.
