require 'spec_helper'

describe 'nextcloud' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with $manage_phplibs = true' do
        let(:params) do
          {
            database_password: 'secret',
            manage_phplibs: true,
          }
        end

        it { is_expected.to compile }
      end

      context 'with $manage_phplibs = false' do
        let(:params) do
          {
            database_password: 'secret',
            manage_phplibs: true,
          }
        end

        it { is_expected.to compile }
      end
    end
  end
end
