require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper unless ENV['BEAKER_provision'] == 'no'
install_module
install_module_dependencies

nextcloud_instance_name = 'example'
nextcloud_docroot = '/var/nextcloud-app'
nextcloud_dbname = "nextcloud_#{nextcloud_instance_name}"
nextcloud_dbuser = "user_#{nextcloud_instance_name}"
nextcloud_dbpasswd = 'secret'

case fact('os.family')
when 'Debian'
  www_user = 'www-data'
  mariadb_client = 'mariadb-client-10.1'
  mariadb_server = 'mariadb-server'
end

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    hosts.each do |host|
      on host, puppet('module', 'install', 'puppetlabs-apache')
      on host, puppet('module', 'install', 'puppetlabs-mysql')
      pp = %(
        package { ['cron','chromium-browser','jq']:
          ensure => present,
        }
        file { '#{nextcloud_docroot}':
          ensure => directory,
          owner  => '#{www_user}',
          group  => '#{www_user}',
          mode   => '0700',
        }
        class { 'apache':
          mpm_module    => 'prefork',
          default_vhost => false,
          default_mods  => ['php','rewrite','headers','env','dir','mime'],
          manage_user   => false,
          manage_group  => false,
          require       => File['#{nextcloud_docroot}'],
        }
        apache::vhost { $facts['networking']['fqdn'] :
          servername    => $facts['networking']['fqdn'],
          port          => '80',
          docroot       => '#{nextcloud_docroot}/nextcloud',
          docroot_owner => '#{www_user}',
          docroot_group => '#{www_user}',
          directories   => [
            {
              path => '#{nextcloud_docroot}/nextcloud',
              allow_override => 'All',
            },
          ],
        }

        class { 'mysql::server':
          package_name     => '#{mariadb_server}',
          service_name     => 'mysql',
          override_options => {
            mysqld => {
              bind_address          => '127.0.0.1',
              transaction_isolation => 'READ-COMMITTED',
              binlog_format         => 'ROW',
              innodb_large_prefix   => true,
              innodb_file_format    => 'barracuda',
              innodb_file_per_table => '1',
            },
          },
          root_password    => 'secretsecret',
        }

        class { 'mysql::client':
          package_name => '#{mariadb_client}',
        }

        mysql::db { '#{nextcloud_dbname}' :
          user     => '#{nextcloud_dbuser}',
          password => '#{nextcloud_dbpasswd}',
          host     => 'localhost',
          grant    => ['ALL'],
          require  => [
            Class['mysql::server'],
          ]
        }
      )
      apply_manifest_on(host, pp, catch_failures: true)
    end
  end
end
