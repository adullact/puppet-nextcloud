require 'spec_helper_acceptance'

fqdn = fact('networking.fqdn')

describe 'nextcloud' do
  context 'with some parameters' do
    pp = %(
      class { 'nextcloud' :
        database_name     => 'nextcloud_example',
        database_user     => 'user_example',
        database_password => 'secret',
        system_user       => 'www-data',
        config            => {
          'trusted_domains'   => [
            'localhost',
            'cloud.example.com',
          ],
          'mail_smtpmode'     => 'smtp',
          'mail_smtphost'     => 'smtp.example.com',
          'mail_smtpport'     => 425,
          'mail_smtpsecure'   => 'tls',
          'mail_smtpauth'     => true,
          'mail_smtpauthtype' => 'LOGIN',
          'mail_smtpname'     => 'username',
          'mail_smtppassword' => 'password',
        },
        additional_apps   => ['unsplash'],
      }
    )
    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
      shell('systemctl restart apache2') # gives apache knowledge of news
    end

    describe command("chromium-browser --no-sandbox --headless --disable-gpu --dump-dom http://#{fqdn}/") do
      its(:stdout) { is_expected.to match %r{.*nextcloud.*} }
    end
    describe command('puppet facts | jq .values.nextcloud.version') do
      its(:stdout) { is_expected.to match %r{^"18\.0\.3} }
    end
    describe command('puppet facts | jq .values.nextcloud_apps.enabled') do
      its(:stdout) { is_expected.to match %r{"activity":\s"\d} }
      its(:stdout) { is_expected.to match %r{"unsplash":\s"\d} }
    end
  end
end
