# @summary handle settings of config.php
#
type Nextcloud::Config = Struct[{
  Optional['trusted_domains']        => Array[String],
  Optional['datadirectory']          => Stdlib::Absolutepath,
  Optional['redis']                  => Nextcloud::Config::Redis,
  Optional['filelocking.enabled']    => Boolean,
  Optional['memcache.locking']       => String,
  Optional['memcache.distributed']   => String,
  Optional['memcache.local']         => String,
  Optional['mail_smtpmode']          => Enum['smtp','sendmail','qmail'],
  Optional['mail_smtphost']          => String,
  Optional['mail_smtpport']          => Integer,
  Optional['mail_smtptimeout']       => Integer,
  Optional['mail_smtpsecure']        => Enum['ssl','tls'],
  Optional['mail_smtpauth']          => Boolean,
  Optional['mail_smtpauthtype']      => Enum['LOGIN','PLAIN'],
  Optional['mail_smtpname']          => String,
  Optional['mail_smtppassword']      => String,
}]
