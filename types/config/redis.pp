# @summary handle redis part of settings in config.php
#
type Nextcloud::Config::Redis = Struct[{
  Optional['host']    => Stdlib::Host,
  Optional['port']    => Stdlib::Port,
  Optional['timeout'] => String,
}]
